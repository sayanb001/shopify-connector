# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{

	'name': 'Common Connector Library',
	'version': '10.0',
    'category': 'Sale',
    'license': 'OPL-1',
    'author': 'prolitus Technologies Pvt. Ltd.',
    'website': 'http://www.prolitus.com',
    'maintainer': 'prolitus Technologies Pvt. Ltd.',

    'description': """
    Develope Generalize Method Of Sale Order,Sale Order Line which is use in any Connector 
    to Create Sale Order and Sale Order Line.
    """,
    'depends': ['delivery','sale_stock'],
    
    'data': [
		'view/stock_quant_package_view.xml',   
        ],
    
    
    'installable': True,
    'active': False,
    
    
    'images': ['static/description/cover.jpg']
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
